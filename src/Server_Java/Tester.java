package Server_Java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Tester {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        Utility utility = new Utility();
        String database = "hangman";
        String username = "root";
        try {
            File fp = new File("src/Server_Java/words.txt");
            BufferedReader br = new BufferedReader(new FileReader(fp));
            String line;
            while ((line = br.readLine()) != null) stringList.add(line);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Register JDBC Driver

        String dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
                + database + "?"
                + "user=" + username + "&"
                + "password=";
        // utility.connect(dbConfig);
        // utility.importWordSet(stringList);
        try {
            utility.login("root", "tree");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(utility.getRemainingAttempts("root"));
        utility.decrementRemainingAttempts("root");
        System.out.println(utility.getRemainingAttempts("root"));
    }
}
