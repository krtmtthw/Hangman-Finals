package Server_Java;

import Corba.HangmanApp.HangmanRemotePOA;
import Corba.HangmanApp.InvalidPasswordException;
import Corba.HangmanApp.UserNotFoundException;

public class HangmanServant extends HangmanRemotePOA {
    @Override
    public void login(String username, String password) throws UserNotFoundException, InvalidPasswordException {
        
    }

    @Override
    public int[] submitGuess(String username, char charGuess) {
        return new int[0];
    }

    @Override
    public int getWordLength(String username) {
        return 0;
    }

    @Override
    public int getRemainingAttempts(String username) {
        return 0;
    }
}
